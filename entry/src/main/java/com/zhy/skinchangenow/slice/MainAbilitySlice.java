package com.zhy.skinchangenow.slice;

import com.zhy.changeskin.SkinManager;
import com.zhy.skinchangenow.ResourceTable;
import com.zhy.skinchangenow.provider.ItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private List<String> dataItems = new ArrayList<String>(Arrays.asList("AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service", "AbilitySlice", "Service",
            "AbilitySlice", "Service", "AbilitySlice", "Service"));

    private ListContainer listContainer;
    private ItemProvider itemProvider;

    private String mSkinPluginFolder = "skin_plugin";   //Plug-in skinning feature folder name to keep the resource images in sdcard storage location /sdcard/skin_plugin

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);
        Button button_loadPlugin = (Button) rootLayout.findComponentById(ResourceTable.Id_id_action_plugin_skinchange);
        Button button_removeSkin = (Button) rootLayout.findComponentById(ResourceTable.Id_id_action_remove_any_skin);
        Button button_modify_listview = (Button) rootLayout.findComponentById(ResourceTable.Id_id_action_notify_lv);
        Button button_testTag = (Button) rootLayout.findComponentById(ResourceTable.Id_id_action_dynamic);
        Button button_inappSkin_red = (Button) rootLayout.findComponentById(ResourceTable.Id_id_inapp_skin_red);
        Button button_inappSkin_green = (Button) rootLayout.findComponentById(ResourceTable.Id_id_inapp_skin_green);

        listContainer = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_list_container);
        initProvider();

        button_loadPlugin.setClickedListener(this);
        button_removeSkin.setClickedListener(this);
        button_modify_listview.setClickedListener(this);
        button_testTag.setClickedListener(this);
        button_inappSkin_red.setClickedListener(this);
        button_inappSkin_green.setClickedListener(this);

        DirectionalLayout directionalLayout = (DirectionalLayout) rootLayout.findComponentById(ResourceTable.Id_main_directionalLayout);
        directionalLayout.setTag("skin:main_bg:background");

        button_loadPlugin.setTag("skin:item_text_color:textColor");
        button_removeSkin.setTag("skin:item_text_color:textColor");
        button_modify_listview.setTag("skin:item_text_color:textColor");
        button_testTag.setTag("skin:item_text_color:textColor");
        button_inappSkin_red.setTag("skin:item_text_color:textColor");
        button_inappSkin_green.setTag("skin:item_text_color:textColor");

        SkinManager.getInstance().register(this, rootLayout);
        super.setUIContent(rootLayout);
    }

    private void initProvider() {
        itemProvider = new ItemProvider(this, dataItems);
        listContainer.setItemProvider(itemProvider);
        listContainer.setReboundEffect(true);
        listContainer.setBoundarySwitch(true);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onClick(Component component) {

        switch (component.getId()) {
            case ResourceTable.Id_id_action_plugin_skinchange:
                ToastDialog toastDialog = new ToastDialog(this);
                SkinManager.getInstance().changeSkin(mSkinPluginFolder, new com.zhy.changeskin.callback.ISkinChangingCallback() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onComplete() {
                        toastDialog.setText("Skinning changed successfully.").show();
                    }

                    @Override
                    public void onError(Exception e) {
                        toastDialog.setText("Skinning change failed.").show();
                    }
                });
                break;
            case ResourceTable.Id_id_action_remove_any_skin:
                SkinManager.getInstance().removeAnySkin();
                break;

            case ResourceTable.Id_id_action_notify_lv:
                for (int i = 0, n = dataItems.size(); i < n; i++) {
                    dataItems.set(i, dataItems.get(i) + " changed");
                }
                itemProvider.notifyDataChanged();
                break;

            case ResourceTable.Id_id_action_dynamic:
                //start the TAG ativity
                Intent intent1 = new Intent();
                intent1.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
                present(new TestTagSlice(), intent1);
                break;

            case ResourceTable.Id_id_inapp_skin_red:
                SkinManager.getInstance().changeSkin("red");
                break;

            case ResourceTable.Id_id_inapp_skin_green:
                SkinManager.getInstance().changeSkin("green");
                break;
            default:
                break;

        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        SkinManager.getInstance().unregister(this);
    }
}