package com.zhy.skinchangenow.slice;

import com.zhy.changeskin.SkinManager;
import com.zhy.skinchangenow.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

public class TestTagSlice extends AbilitySlice {

    ComponentContainer rootLayout;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

         rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_testtag_ability, null, false);
        Button btn_addnewview= (Button) rootLayout.findComponentById(ResourceTable.Id_dynamicaddview);
        Text textHelloWorld = (Text) rootLayout.findComponentById(ResourceTable.Id_textHelloWorld);
        textHelloWorld.setTag("skin:item_text_color:textColor");

        SkinManager.getInstance().register(this,rootLayout);

        btn_addnewview.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                addNewView(component);
            }
        });

        super.setUIContent(rootLayout);
    }

    public void addNewView(Component view) {
        //建议通过xml inflater
        Text tv = new Text(this);
        tv.setTag("skin:item_text_color:textColor");
        tv.setTextColor(new Color(ResourceTable.Color_item_text_color));
        tv.setTextSize(50);
        tv.setText("dynamic add!");

        ((ComponentContainer)rootLayout.findComponentById(ResourceTable.Id_id_container)).addComponent(tv);
        SkinManager.getInstance().injectSkin(tv);

    }


    @Override
    public void onActive() {
        super.onActive();
    }


    @Override
    protected void onStop() {
        super.onStop();
        SkinManager.getInstance().unregister(this);

    }



}