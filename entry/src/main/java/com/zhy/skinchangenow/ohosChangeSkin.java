package com.zhy.skinchangenow;

import com.zhy.changeskin.SkinManager;

import ohos.aafwk.ability.AbilityPackage;

public class ohosChangeSkin extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        SkinManager.getInstance().init(this);
    }
}
