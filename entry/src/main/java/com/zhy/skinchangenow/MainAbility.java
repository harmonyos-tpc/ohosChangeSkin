package com.zhy.skinchangenow;

import com.zhy.skinchangenow.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.security.SystemPermission;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        requestPermission();
    }

    private void requestPermission() {
        List<String> permissions = new LinkedList<>(Arrays.asList(SystemPermission.WRITE_USER_STORAGE,
                SystemPermission.READ_USER_STORAGE, SystemPermission.READ_MEDIA, SystemPermission.WRITE_MEDIA));
        permissions.removeIf(
                permission -> verifySelfPermission(permission) == PERMISSION_GRANTED || !canRequestPermission(permission));

        if (!permissions.isEmpty()) {
            requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]), 1);
        }
    }
}
