package com.zhy.skinchangenow.provider;

import com.zhy.changeskin.SkinManager;
import com.zhy.skinchangenow.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class ItemProvider extends BaseItemProvider {

    private Context context;
    private List<String> listData;
    public ItemProvider(Context context, List<String> data) {
        this.context = context;
        this.listData = data;
    }
    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component container = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item, null, false);
        Image image = (Image) container.findComponentById(ResourceTable.Id_id_iv_icon);
        image.setTag("skin:left_menu_icon:src");

        Text title = (Text) container.findComponentById(ResourceTable.Id_id_tv_title);
        title.setTag("skin:item_text_color:textColor");
        title.setText(listData.get(position));

        Text tip = (Text) container.findComponentById(ResourceTable.Id_id_tv_tip);
        tip.setTag("skin:item_text_color:textColor");

        SkinManager.getInstance().injectSkin(container);

        return container;
    }
}
