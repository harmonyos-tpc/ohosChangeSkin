package com.zhy.changeskin.attr;


import ohos.agp.components.Component;

public class SkinAttr
{
    public String resName;
    public SkinAttrType attrType;


    public SkinAttr(SkinAttrType attrType, String resName)
    {
        this.resName = resName;
        this.attrType = attrType;
    }

    public void apply(Component Component)
    {
        attrType.apply(Component, resName);
    }
}
