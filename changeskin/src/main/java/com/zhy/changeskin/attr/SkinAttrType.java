package com.zhy.changeskin.attr;

import com.zhy.changeskin.ResourceManager;
import com.zhy.changeskin.SkinManager;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;

public enum SkinAttrType
{
    BACKGROUND("background")
        {
            @Override
            public void apply(Component component, String resName) {
                Element drawable = getResourceManager().getDrawableByName(resName);
                if (drawable != null) {
                    component.setBackground(drawable);
                } else {
                    int color = getResourceManager().getColor(resName);
                    if (color == 0)
                        return;
                    ShapeElement elementColor = new ShapeElement();
                    RgbColor rgbColor = RgbColor.fromArgbInt(color);
                    elementColor.setRgbColor(rgbColor);
                    component.setBackground(elementColor);
                }
            }
        }, COLOR("textColor")
        {
            @Override
            public void apply(Component component, String resName) {
                int color = getResourceManager().getColor(resName);
                if (color == 0)
                    return;
                ((Text) component).setTextColor(new Color(color));
            }
        }, SRC("src")
        {
            @Override
            public void apply(Component component, String resName)
            {
                if (component instanceof Image) {
                    PixelMap pixelmap = getResourceManager().getPixelMapByName(resName);
                    if (pixelmap == null) return;
                    ((Image) component).setPixelMap(pixelmap);
                }

            }
        }, DIVIDER("divider")
	    {
            @Override
            public void apply(Component component, String resName) {
                if (component instanceof ListContainer) {
                    Element divider = getResourceManager().getDrawableByName(resName);
                    if (divider == null) return;
                   ((ListContainer) component).setBoundary(divider);
                }
            }
        };

    String attrType;

    SkinAttrType(String attrType) {
        this.attrType = attrType;
    }

    public String getAttrType() {
        return attrType;
    }


    public abstract void apply(Component component, String resName);

    public ResourceManager getResourceManager() {
        return SkinManager.getInstance().getResourceManager();
    }

}
