package com.zhy.changeskin.attr;

import ohos.agp.components.Component;

import java.util.List;

public class SkinView {
    public Component view ;
    public List<SkinAttr> attrs;

    public SkinView(Component component, List<SkinAttr> skinAttrs) {
        this.view = component;
        this.attrs = skinAttrs;
    }

    public void apply() {
        if (view == null) return;
        for (SkinAttr attr : attrs) {
            attr.apply(view);
        }
    }
}
