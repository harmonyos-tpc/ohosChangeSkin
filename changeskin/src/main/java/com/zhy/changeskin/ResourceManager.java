package com.zhy.changeskin;


import com.zhy.changeskin.utils.ResUtil;
import com.zhy.changeskin.utils.TextUtils;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Optional;

public class ResourceManager {
    private Context mContext;
    private String mSuffix;
    private static final String FORMAT_HINT = "image/png";

    public ResourceManager(Context context, String suffix) {
        mContext = context;
        if (suffix == null) {
            suffix = "";
        }
        mSuffix = suffix;
    }

    public Element getDrawableByName(String name) {
        String resName = "Media_"+appendSuffix(name);
        boolean usePlugin = SkinManager.getInstance().isUsePlugin();

        if(!usePlugin && getFieldValue(resName) != null) {
            int imageResource = (int) getFieldValue(resName);
            return ResUtil.getPixelMapDrawable(mContext, imageResource);
        } else if (usePlugin) {
            HashMap<String, String> resMap = SkinManager.getInstance().getResourceMap();
            if(resMap !=null && resMap.containsKey(name)) {
                Uri imageUri = Uri.parse(resMap.get(name));
                PixelMap pixelMap = getPixelMapByUri(imageUri);
                if  (pixelMap != null)
                    return new PixelMapElement(pixelMap);
            }
        }
        return null;
    }

    public PixelMap getPixelMapByName(String name) {
        String resName = "Media_"+appendSuffix(name);
        boolean usePlugin = SkinManager.getInstance().isUsePlugin();

        if(!usePlugin && getFieldValue(resName) != null) {
            int imageResource = (int) getFieldValue(resName);
            return ResUtil.getPixelMap(mContext, imageResource).get();
        } else if (usePlugin) {
            HashMap<String, String> resMap = SkinManager.getInstance().getResourceMap();
            if(resMap !=null && resMap.containsKey(name)) {
                Uri imageUri = Uri.parse(resMap.get(name));
                return getPixelMapByUri(imageUri);
            }
        }
        return null;
    }

    private Object getFieldValue(String name) {
        Field fieldName = null;
        try {
            Class<?> resourceTable = Class.forName(mContext.getBundleName() + ".ResourceTable");
            fieldName = resourceTable.getField(name);
            return fieldName.get(null);
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getColor(String name) {
        String resName = "Color_"+appendSuffix(name);
        if (getFieldValue(resName) != null) {
            int colorResource = (int) getFieldValue(resName);
            return ResUtil.getColor(mContext, colorResource);
        }
        return 0;
    }

    private String appendSuffix(String name) {
        if (!TextUtils.isEmpty(mSuffix))
            return name += "_" + mSuffix;
        return name;
    }

    public PixelMap getPixelMapByUri(Uri imageUri) {
        DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(mContext);
        try {
            FileDescriptor fd = dataAbilityHelper.openFile(imageUri, "r");
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = FORMAT_HINT;
            ImageSource source = ImageSource.create(fd, options);
            PixelMap pixelMap =  Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
            return pixelMap;
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
   }
}
