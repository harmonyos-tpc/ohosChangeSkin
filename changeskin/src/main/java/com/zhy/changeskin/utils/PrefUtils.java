package com.zhy.changeskin.utils;

import com.zhy.changeskin.constant.SkinConfig;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

public class PrefUtils {
    private Context mContext;
    private DatabaseHelper mDatabaseHelper;

    public PrefUtils(Context context) {
        this.mContext = context;
        mDatabaseHelper = new DatabaseHelper(context);
    }

    public String getPluginPath() {
        Preferences sp = mDatabaseHelper.getPreferences(SkinConfig.PREF_NAME);
        return sp.getString(SkinConfig.KEY_PLUGIN_PATH, "");
    }

    public String getSuffix() {
        Preferences sp = mDatabaseHelper.getPreferences(SkinConfig.PREF_NAME);
        return sp.getString(SkinConfig.KEY_PLUGIN_SUFFIX, "");
    }

    public boolean clear() {
        Preferences sp = mDatabaseHelper.getPreferences(SkinConfig.PREF_NAME);
        return sp.clear().flushSync();
    }

    public void putPluginPath(String path) {
        Preferences sp = mDatabaseHelper.getPreferences(SkinConfig.PREF_NAME);
        sp.putString(SkinConfig.KEY_PLUGIN_PATH, path).flush();
    }

    public void putPluginSuffix(String suffix) {
        Preferences sp = mDatabaseHelper.getPreferences(SkinConfig.PREF_NAME);
        sp.putString(SkinConfig.KEY_PLUGIN_SUFFIX, suffix).flush();
    }

}
