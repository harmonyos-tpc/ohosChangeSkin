/*
 *  * Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.zhy.changeskin.utils;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.ability.DataUriUtils;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.util.HashMap;

public class MediaUtil {


    public HashMap<String, String> queryMedia(String skinPluginFolder, Context context) {
        HashMap<String, String> imageResMap = new HashMap<String, String>();
        try {
            DataAbilityHelper dataAbilityHelper = DataAbilityHelper.creator(context);
            ResultSet resultSet = dataAbilityHelper.query(getScanUri(), getProjection(), null);
            if (resultSet != null) {
                while (resultSet.goToNextRow()) {
                    parse(skinPluginFolder, resultSet, imageResMap);
                }
                resultSet.close();
            }
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imageResMap;
    }

    private Uri getScanUri() {
        return AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
    }

    private String[] getProjection() {
        return new String[]{
            AVStorage.Images.Media.DATA,
            AVStorage.Images.Media.MIME_TYPE,
            AVStorage.Images.Media.ID,
            AVStorage.Images.Media.DISPLAY_NAME,
            AVStorage.Images.Media.DATE_ADDED
        };
    }

    private void parse(String skinPluginFolder, ResultSet cursor, HashMap<String, String> imageResMap) {
        String path = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.DATA));
        String mime = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.MIME_TYPE));
        int fileId = cursor.getInt(cursor.getColumnIndexForName(AVStorage.Images.Media.ID));
        String fileName = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.DISPLAY_NAME));
        long dateToken = cursor.getLong(cursor.getColumnIndexForName(AVStorage.Images.Media.DATE_ADDED));
        Uri externalUri = DataUriUtils.attachId(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, fileId);

        if (path.contains(skinPluginFolder)) {
            String[] filename =  fileName.split("\\.");
            if (filename.length != 0 ) {
                imageResMap.put(filename[0], externalUri.toString());
            }
        }
    }
}
