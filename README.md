# ohosChangeSkin

A completely non-intrusive skinning method, supports plug-in and in-app, without restarting AbilitySlice.

## Features

* Plug-in skinning (Supports skinning only for image resources using src, background)
* In-app skinning
* Support dynamic generation of addView

# Installation Instructions
1.For using ohosChangeSkin module in sample app, include the source code and add the below dependencies in entry/build.gradle to generate hap/ohosChangeSkin.har.

```
dependencies {
    implementation project(':changeskin')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
    }
```

2.For using ohosChangeSkin in separate application using har file, add the har file in the entry/libs folder and add the dependencies in entry/build.gradle file.

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
}
```

3.For using ohosChangeSkin from a remote repository in separate application, add the below dependency in entry/build.gradle file.

```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohosChangeSkin:1.0.0'
}
```

## Usage Instructions

* Application

Called in Application, SkinManager.getInstance().init(this);

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        SkinManager.getInstance().init(this);
    }
}


* Ability

In the onStart and onStop of the AbilitySlice that needs to be skinned, respectively:

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                        .parse(ResourceTable.Layout_ability_main, null, false);
	    SkinManager.getInstance().register(this, rootLayout);
    }


    @Override
    protected void onStop() {
        super.onStop();
        SkinManager.getInstance().unregister(this);
    }


Due to platform limitation, add the tag attribute to the component using Component.setTag(String) API in the code. Example,

    DirectionalLayout directionalLayout = (DirectionalLayout) rootLayout.findComponentById(ResourceTable.Id_main_directionalLayout);
    directionalLayout.setTag("skin:main_bg:background");

The tag attribute is divided into 3 parts:

* skin
* The name of the resource, that is, the name of the resource in the plug-in package, needs to be consistent with the resource name used in the current app.
* Supported attributes, currently supports src, background, textColor, and divider.

For multiple attributes of a Component that need to be skinned, setTag("skin:item_text_color:textColor|skin:icon:src") use | to separate them.

In short: if you need to change the skin of which View, add the tag attribute, and the tag value can be set as described above.

* Skinned API

Plug-in:
    Keep the resource images in skin_plugin to sdcard (internal) storage location /sdcard/skin_plugin. And provide Storage permission
    when requested from application to access resources in sdcard.

    Limitation: Requires reboot of the device or emulator to read the images in sdcard (internal)

    ToastDialog toastDialog = new ToastDialog(this);
    SkinManager.getInstance().changeSkin(
        mSkinPluginFolder,
        new ISkinChangingCallback() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(Exception e) {
                toastDialog.setText("Skinning change failed.").show();
            }

            @Override
            public void onComplete() {
                toastDialog.setText("Skinning changed successfully.").show();
            }
        });
<img src="./images/plug-in_skin.png" width=450 ></img>


In-app:

    SkinManager.getInstance().changeSkin(suffix);

    The suffixes of multiple skins in the application can be distinguished, such as main_bg, the skin resources can be: main_bg_red, main_bg_green, etc.

    When changing the skin, directly pass in the suffix, such as red, green as described above.

Green:
    <img src="./images/inapp_skin_green.png" width=450 ></img>

Red:
    <img src="./images/inapp_skin_red.png" width=450 ></img>

Default Skin:
    <img src="./images/default_skin.png" width=450 ></img>


Dynamic Addview:
    <img src="./images/dynamic_addview.png" width=450 ></img>

Modifying Listview Data:
    <img src="./images/modify_listviewdata.png" width=450 ></img>